require 'chef/provider'

module ChefInstance
  module Provider
    # Provides a Superclass Chef Provider to help with Factories.
    class Instance < Chef::Provider
      # Construct a new ChefInstance::Provider::Instance.
      # @param new_resource [Chef::Resource]
      # @param run_context [Chef::RunContext]
      def initialize(new_resource, run_context = nil)
        super
        @new_resource = new_resource
        @run_context = run_context
        @namespace = @new_resource.namespace
      end

      # http://docs.opscode.com/essentials_nodes_why_run.html
      def whyrun_supported?
        false
      end

      # Determines the state of the resource _currently_
      # @return [Resource] @current_resource with name of @new_resource
      def load_current_resource
        @current_resource = Chef::Resource::Instance.new(
          @new_resource, @run_context)
      end

      # Installs the `install_type`
      # Creates the `service_type`
      def action_create
        instance(@new_resource.install_type, :install)
        instance(@new_resource.service_type, :create)
      end

      # Destroy the `service_type`
      # Uninstall the `install_type`
      def action_destroy
      end

      # Disable the `service_type`
      def action_disable
        instance(@new_resource.service_type, :disable)
      end

      # Enable the `service_type`
      def action_enable
        instance(@new_resource.service_type, :enable)
      end

      private

      # Lookup Class, Construct, and send an action.
      # @param type [String] The install or service type being used.
      # @param action [Symbol] The action being performed.
      # return [TrueClass, FalseClass]
      def instance(type, action)
        instance_class = instance_sub_class(type)
        i = instance_class.new(@new_resource, @run_context)
        i.send(action)
      end

      # Lookup the CONSTANT name of a class
      # ChefInstance::Service::Init == BaseName::Type::Name
      # @param type [String] The type to lookup.
      # @param name [String] The name of the type to lookup.
      # return [Constant] The class handling type.
      def instance_sub_class(type, name)
        klass = "#{ @namespace }::#{ type.capitalize }::#{ name.capitalize }"
        klass.split('::').reduce(Object) { |a, e| a.const_get(e) }
      end

      # Determine the @current_resource service provider.
      def discover_service
      end

      # Determine how the @current_resource is installed.
      def discover_install
      end
    end
  end
end
