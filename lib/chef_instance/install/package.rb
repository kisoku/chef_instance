require 'chef/resource/package'
require 'chef_instance/install'

module ChefInstance
  module Install
    # A Superclass template for installing packages.
    class Package < ChefInstance::Install::Template
      # Construct a new ChefInstance::Install::Package.
      # @param new_resource [Chef::Resource]
      # @param run_context [Chef::RunContext]
      def initialize(new_resource, run_context = nil)
        super
      end

      private

      def package_resource
        @package ||= Chef::Resource::Package.new(package_name)
      end
    end
  end
end
