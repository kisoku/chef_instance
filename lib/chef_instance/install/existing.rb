require 'chef_instance/install'

module ChefInstance
  module Install
    # A Superclass for installing softare that already exists on the system.
    # Useful for software copied on to the system from the outside, or
    # available on the local disk.
    class Existing < ChefInstance::Install::Template
      # Construct a new Existing object.
      # @param new_resource [Chef::Resource]
      # @param run_context [Chef::RunContext]
      def initialize(new_resource, run_context = nil)
        super
        @new_resource = new_resource
        @run_context = run_context
      end

      # We don't really install since its already existing.
      def install
      end

      # We don't really uninstall since its already existing.
      def uninstall
      end
    end
  end
end
