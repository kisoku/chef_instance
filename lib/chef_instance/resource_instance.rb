require 'chef/resource'

module ChefInstance
  module Resource
    # Provides a Superclass Chef Resource to help with Factories.
    class Instance < Chef::Resource
      # Construct a new ChefInstance::Resource::Instance.
      # @param new_resource [Chef::Resource]
      # @param run_context [Chef::RunContext]
      def initialize(new_resource, run_context = nil)
        super
        @provider = ChefInstance::Provider::Instance
        @action = :create
        @allowed_actions = [:create, :destroy, :enable, :disable, :nothing]
      end

      # A hash passed to the install type.
      # Used to pass end-user overrides to the installer.
      # @param arg [Hash]
      def install_options(arg = nil)
        set_or_return(:install_options, arg, kind_of: Hash)
      end

      # The type of installation you are doing.
      # @param arg [Symbol]
      def install_type(arg = nil)
        set_or_return(:install_type,
                      arg,
                      kind_of: [Symbol],
                      required: true,
                      equal_to: [:package, :existing])
      end

      # A hash passed to the service type.
      # Used to pass end-user overrides to the service.
      # @param arg [Hash]
      def service_options(arg = nil)
        set_or_return(:service_options, arg, kind_of: Hash)
      end

      # The type of service manager you are using.
      # @param arg [Symbol]
      def service_type(arg = nil)
        set_or_return(:service_type,
                      arg,
                      kind_of: Symbol,
                      required: true,
                      equal_to: [:init, :runit])
      end

      # Provide the namespace for lookups.
      # Because the provider subclasses Chef::Provider
      # Its `self.class` points us in the wrong direction.
      # @param [String] namespace to use for lookups.
      def namespace(arg = nil)
        set_or_return(:namespace,
                      arg,
                      kind_of: String,
                      default: 'ChefInstance')

      end
    end
  end
end
