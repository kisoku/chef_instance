module ChefInstance
  module Install
    # A Superclass template for installing software.
    class Template
      # Construct a new ChefInstance::Install::Template.
      # @param new_resource [Chef::Resource]
      # @param run_context [Chef::RunContext]
      def initialize(new_resource, run_context = nil)
        @new_resource = new_resource
        @run_context = run_context
      end

      def install
      end

      def uninstall
      end

      private

    end
  end
end
