module ChefInstance
  module Service
    # A Superclass template for service managers.
    class Template
      # Construct a new ChefInstance::Service::Template.
      # @param new_resource [Chef::Resource]
      # @param run_context [Chef::RunContext]
      def initialize(new_resource, run_context = nil)
        @new_resource = new_resource
        @run_context = run_context
      end

      # Create and place the service script.
      def create
        create_service_script
      end

      # Destroy the service script.
      def destroy
        destroy_service
      end

      # If enabled, disable the service.
      def disable
        disable_service
      end

      # Enable the service.
      def enable
        enable_service
      end

      private

      # Enable a service if it has been configured.
      def enable_service
        ls_dir = logstash_conf_dir(@new_resource.conf_dir, @new_resource.name)
        if ::File.directory?(ls_dir)
          if logstash_has_configs?(ls_dir)
            s = Chef::Resource::Service.new(ls_svc, @run_context)
            s.run_action([:enable, :start])
          end
        end
      end
    end
  end
end
