require 'chef_instance/service'

module ChefInstance
  module Service
    # A Superclass managing Runit.
    class Runit < ChefInstance::Service::Template
      # Construct a new ChefInstance::Service::Runit.
      # @param new_resource [Chef::Resource]
      # @param run_context [Chef::RunContext]
      def initialize(new_resource, run_context = nil)
        super
        @new_resource = new_resource
        @run_context = run_context
      end

      private

      def create_service_script
        Chef::Resource::RunitService.new(@new_resource, @run_context)
      end
    end
  end
end
