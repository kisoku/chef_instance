require 'chef/resource/template'
require 'chef_instance/service'

module ChefInstance
  module Service
    # A Superclass managing Init.
    class Init < ChefInstance::Service::Template
      # Construct a new ChefInstance::Service::Init.
      # @param new_resource [Chef::Resource]
      # @param run_context [Chef::RunContext]
      def initialize(new_resource, run_context = nil)
        super
      end

      private

      # Uses a template to create a Init script for the named service.
      def create_service_script
        #r = Chef::Resource::Template.new
      end
    end
  end
end
