# coding: utf-8
require_relative 'lib/chef_instance/version'

Gem::Specification.new do |spec|
  spec.name          = 'chef_instance'
  spec.version       = ChefInstance::VERSION
  spec.authors       = ['Miah Johnson']
  spec.email         = %w(miah@chia-pet.org)
  spec.description   = 'Chef Resource & Provider for building software' \
                       'instances.'
  spec.summary       = 'Chef Resource & Provider Super Classes for' \
                       'building stand-alone instances of software.'
  spec.homepage      = 'https://github.com/miah/chef_instance'
  spec.license       = 'Apache-2.0'
  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r(^bin/)) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r(^(test|spec|features)/))
  spec.require_paths = %w(lib)

  spec.add_runtime_dependency 'chef', '>= 11.12.2'
  spec.add_development_dependency 'codeclimate-test-reporter', '~> 0.3.0'
  spec.add_development_dependency 'minitest', '~> 5.3.1'
  spec.add_development_dependency 'rake', '~> 10.3.0'
  spec.add_development_dependency 'rubocop', '~> 0.20.1'
  spec.add_development_dependency 'simplecov', '~> 0.8.2'
  spec.add_development_dependency 'yard', '~> 0.8.7.3'
end
