unless ENV['TRAVIS']
  require 'simplecov'
  SimpleCov.start do
    add_filter '/.gems/'
    add_filter '/test/'
  end
end

if ENV['CODECLIMATE_REPO_TOKEN']
  require 'codeclimate-test-reporter'
  CodeClimate::TestReporter.start
end

require 'chef/event_dispatch/dispatcher'
require 'chef/platform'
require 'chef/run_context'
require 'chef/resource'
require 'chef/resource/service'
require 'chef/provider'
require 'minitest/autorun'

Minitest::Test.parallelize_me!
