require_relative 'spec_helper'
require 'chef_instance/install'

describe ChefInstance::Install, '' do
  let(:node) do
    node = Chef::Node.new
    node.automatic[:platform] = 'ubuntu'
    node.automatic[:platform_version] = '12.04'
    node
  end

  let(:events) { Chef::EventDispatch::Dispatcher.new }
  let(:run_context) { Chef::RunContext.new(node, {}, events) }
  let(:resource) { 'package_test' }
  let(:install) do
    ChefInstance::Install::Template.new(resource, run_context)
  end

  before do
    @install_res = install
  end

  describe 'Object Ancestry Checks' do
    it 'Is a ChefInstance::Install::Template?' do
      @install_res.must_be_kind_of(ChefInstance::Install::Template)
    end

    it 'Is a instance of Init' do
      @install_res.must_be_instance_of(ChefInstance::Install::Template)
    end
  end

  describe 'It adhears to the Install::Template contract.' do

    describe 'It should..' do
      it 'Create.' do
        @install_res.must_respond_to(:install)
      end

      it 'Destroy.' do
        @install_res.must_respond_to(:uninstall)
      end
    end
  end

end
