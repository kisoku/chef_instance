require_relative '../spec_helper'
require 'chef_instance/service/runit'

describe ChefInstance::Service::Runit, '' do

  let(:node) do
    node = Chef::Node.new
    node.automatic[:platform] = 'ubuntu'
    node.automatic[:platform_version] = '12.04'
    node
  end

  let(:events) { Chef::EventDispatch::Dispatcher.new }
  let(:run_context) { Chef::RunContext.new(node, {}, events) }
  let(:resource) { 'runit_test' }
  let(:service) do
    ChefInstance::Service::Runit.new(resource, run_context)
  end

  before do
    @service_res = service
  end

  describe 'Object Ancestry Checks' do
    it 'Is a ChefInstance::Service::Template?' do
      @service_res.must_be_kind_of(ChefInstance::Service::Template)
    end

    it 'Is a instance of Init' do
      @service_res.must_be_instance_of(ChefInstance::Service::Runit)
    end
  end

  describe 'It adhears to the Service::Template contract.' do

    describe 'It should..' do
      it 'Create.' do
        @service_res.must_respond_to(:create)
      end

      it 'Destroy.' do
        @service_res.must_respond_to(:destroy)
      end

      it 'Enable.' do
        @service_res.must_respond_to(:enable)
      end

      it 'Disable.' do
        @service_res.must_respond_to(:disable)
      end
    end
  end
end
